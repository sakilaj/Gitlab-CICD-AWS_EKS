# DevOps GitLab CI/CD on AWS Cloud
***DevOps by gitlab kubernetes Sep 5,2018*** by
*DITDANAI JAIKAM*
[G-Able-ITO](https://www.g-able.com/)
*Software Engineer* *System Analyst* *Beginer to Devops Engineer* | 

[![N|Solid](https://cdn-images-1.medium.com/max/640/1*W0RZgzz8d1SUymf0aStFcg.png)](https://nodesource.com/products/nsolid)

**How to config and prepare a AWS environments** [Install-link](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html)

- [GITLAB account](https://gitlab.com/)
- [AWS account](https://aws.amazon.com/th/)

**Getting Started with Amazon EKS**

- Create new user even thought root account
- [Open the IAM console at](https://aws.amazon.com/th/iam/?nc2=h_m1)(we should setting AWS IAM on AWS)
- CREATE GROUP,USER,ROLE and add permission |GROUP|USER|

> ExampJSON
```sh 
    {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "eks:*"
            ],
            "Resource": "*"
        }
    ]
}

``` 
- *Recommend enable MFA**

- Amazon EKS uses IAM to provide authentication to your Kubernetes cluster [*Role Based Access Control*](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)

[![N|Solid](https://docs.aws.amazon.com/eks/latest/userguide/images/eks-iam.png)](https://docs.aws.amazon.com/eks/latest/userguide/managing-auth.html)

- [Create your Amazon EKS cluster VPC](https://console.aws.amazon.com/cloudformation) *security network*
 
```sh
$curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/kubectl
$chmod +x ./kubectl
$cp ./kubectl /usr/local/bin/kubectll && export PATH=/usr/local/bin/kubectl/bin:$PATH
$echo 'export PATH=/usr/local/bin:$PATH' >> ~/.bashrc
#you can verify its version with the following command
$kubectl version --short --client
#output
Client Version: v1.11.2
```
To install aws-iam-authenticator for Amazon EKS(linux)
```sh
$curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/linux/amd64/aws-iam-authenticator
$chmod +x ./aws-iam-authenticator
$cp ./aws-iam-authenticator /usr/local/bin/aws-iam-authenticator && export PATH=/usr/local/bin:$PATH
$echo 'export PATH=/usr/local/bin:$PATH' >> ~/.bashrc
#Test output
$aws-iam-authenticator help
```
(Optiional) Installing the AWS Command Line Interface https://docs.aws.amazon.com/cli/latest/userguide/installing.html

> The AWS CLI is an open source tool built on top of the AWS SDK for Python (Boto) that provides commands for interacting with AWS services. With minimal configuration, you can start using all of the functionality provided by the AWS Management Console from your favorite terminal program

### Step 1: Create Your Amazon EKS Cluster
- [Configuring the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) : Access key & Access Key ID from AWS IAM(user) 
- [Open the Amazon EKS console](https://console.aws.amazon.com/eks/home#/clusters.)

```sh 

$aws eks describe-cluster --name <cluster> --query cluster.status
$aws eks describe-cluster --name <clustre>  --query cluster.endpoint 
--output http://yourcluster
$aws eks describe-cluster --name <cluster> --query cluster.certificateAuthority.data --output text

```
> Warning : make sure the region cluster 
$sudo nano ~/.aws/config
 [default]
output = json
region = us-east-1

### Step 2: Configure kubectl for Amazon EKS

```sh
$aws sts get-caller-identity
$mkdir -p ~/.kube
$sudo nano ~/.kub/config
# make file below

```
*config<name-cluster>*
```sh 
apiVersion: v1
clusters:
- cluster:
    server: <endpoint-url>
    certificate-authority-data: <base64-encoded-ca-cert>
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "<cluster-name>"
        # - "-r"
        # - "<role-arn>"
      # env:
        # - name: AWS_PROFILE
        #   value: "<aws-profile>"
```
Export file config to kubernetes

```sh
$export KUBECONFIG=$KUBECONFIG:~/.kube/config-<name>
$echo 'export KUBECONFIG=$KUBECONFIG:~/.kube/config-<name>' >> ~/.bashrc
$kubectl get svc
```
Expected Output:
```sh
NAME             TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
svc/kubernetes   ClusterIP   10.100.0.1   <none>        443/TCP   1m
```
### Step 3: Launch and Configure Amazon EKS Worker Nodes
- [Open the AWS CloudFormation console](https://console.aws.amazon.com/cloudformation)
- [How to Config](https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html)
To enable worker nodes to join your cluster

```sh
$curl -O https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2018-08-30/aws-auth-cm.yaml
```
Replace the <ARN of instance role (not instance profile)> **You will have NodeInstanceRole After you created Amazon EKS - Node Group to completely**
> !You need keyname in EC2 generated 

```sh
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: <ARN of instance role (not instance profile)>
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
```
Next command
```sh
$kubectl apply -f aws-auth-cm.yaml
$kubectl get nodes --watch

```
### STEP 4:Tutorial: Deploy the Kubernetes Web UI (Dashboard)
- [You can use Dashboard](https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html) 
- [![N|Solid](https://www.picz.in.th/images/2018/09/06/fQgFOf.png)](https://www.picz.in.th/image/kubernetes.fQgFOf)

### STEP 5:Gitlab CICD 

- Download tool vscode or linux command *$sudo snap install vscode*
- [Create your Project. after that select in side bar to Kubernetes](https://docs.gitlab.com/ee/user/project/clusters/eks_and_gitlab/) 
- add .gitlab-ci.yaml,kubernetes.tpl.yml

> **importance** you needs install helm into kubernetes cluster and Gitlab Runner, if you want to CICD on GILAB to AWS

```sh
#run
image: docker:latest
services:
  - docker:dind
# variable to  code 
variables:
  DOCKER_DRIVER: overlay
  CONTAINER_DEV_IMAGE: registry.gitlab.com/ito-community-team/ito-community-web-authentication:$CI_COMMIT_SHA
#stage code 
stages:
  - test
  - build-artifact
  - deploy
#test build .net 
testing:
  image: microsoft/dotnet:2.1-sdk 
  stage: test
  before_script:
    - dotnet restore
  script:
    - dotnet publish -c Release -o out
  only:
    - master
#build artifact docker container
build:
  stage: build-artifact

  before_script:
    - docker --version
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker build -f Docker/Dockerfile --pull -t $CONTAINER_DEV_IMAGE .
    - docker push $CONTAINER_DEV_IMAGE
  only:
    - master
#deploy code on stagging new 
deploy_staging:
  stage: deploy
  image: alpine
  environment:
    name: master
   #url: http://35.193.40.82
  script:
    - apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl
    - kubectl config set-cluster ito-team --server="$KUBE_URL" --insecure-skip-tls-verify=true
    - kubectl config set-credentials admin --token="$KUBE_TOKEN"
   #- kubectl config set-credentials admin --username="$KUBE_USER" --password="$KUBE_PASSWORD"
    - kubectl config set-context default --cluster=ito-team --user=admin
    - kubectl config use-context default
    - 'printf "apiVersion: v1\nkind: Secret\n$(kubectl create secret docker-registry gitlab-registry --docker-server=$CI_REGISTRY --docker-username=$CI_REGISTRY_USER --docker-password=$CI_REGISTRY_PASSWORD --docker-email=$GITLAB_USER_EMAIL -o yaml --dry-run)" | kubectl apply -f -'
    -  sed 's/_APP_NAME_/'"$CI_PROJECT_NAME"'/g; s/_VERSION_/'"$CI_COMMIT_SHA"'/g' kubernetes.tpl.yml > kubernetes.yml;
    - kubectl apply -f kubernetes.yml
  only:
    - master
    
```
> .kubernetes.tpl.yml

```sh
apiVersion: apps/v1
kind: Deployment
metadata:
  name: _APP_NAME_
  labels:
    app: _APP_NAME_
spec:
  replicas: 1
  selector:
    matchLabels:
      app: _APP_NAME_
  
  # Pod template
  template:
    metadata:
      labels:
        app: _APP_NAME_
    spec:
      containers:
        - name: test
          image: registry.gitlab.com/<your project>:_VERSION_
          imagePullPolicy: Always
          ports:
            - containerPort: 80
      imagePullSecrets:
        - name: gitlab-registry
---
apiVersion: v1
kind: Service
metadata:
  name: _APP_NAME_
  labels:
    app: _APP_NAME_
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
      app: _APP_NAME_

```
Output
![N|Solid](https://www.picz.in.th/images/2018/09/06/fQiQ4g.jpg)

### Command
| Command | Description |
| ------ | ------ |
| $aws configure | show default aws config|
| $aws sts get-caller-identity | show credentials aws account 
| $sudo nano ~/.aws/credentials | aws show credentials (aws_secret_access_key,aws_access_key_id)|
| $sudo nano ~/.aws/config|aws show profile|
| $kubectl get secrets| show secret |
| $kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | get ca certification client | [plugins/medium/README.md][PlMe] |
| $kubectl get secret <secret name> -o jsonpath="{['data']['token']}" |get token(use to gitlab Add an existing Kubernetes cluster) | 
| $kubectl config current-context | context for cluster config
| $kubectl config view| view a specified kubeconfig file
|$kubectl get cm --all-namespaces| show Kubernetes all name space 
|$kubectl get nodes --watch | use went suscess config to kubernetes-worker-node-l
|$ kubectl cluster-info|show cluster end-point connect




  
